/**
 * @file
 * Defines the behaviors needed for cropper integration.
 */

(function ($, Drupal, window) {
  Drupal.behaviors.testingConsole = {
    attach: function (context, settings) {
      if(context === document) {
        $('#edit-inject-paragraph').mousedown(function(){
          $('.field--name-field-image-library').css('display','none');
          $('.field--name-field-image-library-paragraphs').css('display','block');
          
        })
      }
    }
  }

  Drupal.behaviors.globalAltAndCopyright = {
    attach: function (context, settings) {
      $('.media-library-add-form__media .field--name-name input').prop('readonly', true);
      $('.media-library-add-form__media .field--name-name input').css({
          'background-color':'#ebebe4',
          'pointer-events':'none'
        });
      $('.global-alt-copy-transfer-btn').mousedown(function(){
          
            // console.log("gi click");
            let num_of_items = $('.media-library-add-form__media');
            let global_alt_value = $('.form-item-global-alt input').val();
            let global_copyright_value = $('.form-item-global-copyright input').val();
            // console.log(global_alt_value);
            // console.log(global_copyright_value);
          
            num_of_items.each(function() {
              
              $('.media-library-add-form__media .field--name-field-alt input').val(global_alt_value);
              $('.media-library-add-form__media .field--name-field-copyright input').val(global_copyright_value);
            })
          
        })
      
      if(context === document) {

        // Attach a click listener to the clear button.
        // var clearBtn = document.getElementById('edit-transfer-values--yGSdNRFMpp0');
        // clearBtn.addEventListener('click', function() {

        //     // Do something!
        //     console.log('Clear button clicked!');

        // }, false);
        // console.log("gi click");
        // $('.media-library-open-button').mousedown(function(){
        //   // if ($('div.media-library-add-form--with-input').length) {
        //     console.log("gi click");
        //   // }
        // })
          // $('#drupal-modal .media-library-add-form .media-library-add-form__description').after('<div>test</div>');

          // var ajaxSettings = {
          //   // url: '',
          //   // If the old version of Drupal.ajax() needs to be used those
          //   // properties can be added
          //   // base: 'myBase',
          //   element: $(context).find('.media-library-add-form__description')
          // };
        
          // var myAjaxObject = Drupal.ajax(ajaxSettings);
        
          // // Declare a new Ajax command specifically for this Ajax object.
          // myAjaxObject.commands.insert = function (ajax, response, status) {
          //   // $('#my-wrapper').append(response.data);
          //   alert('test');
          // };
      
          // // This command will remove this Ajax object from the page.
          // myAjaxObject.commands.destroyObject = function (ajax, response, status) {
          //   Drupal.ajax.instances[this.instanceIndex] = null;
          // };
    
          // // Programmatically trigger the Ajax request.
          // myAjaxObject.execute();
            
          
      }
    }
  }

  Drupal.behaviors.inlineVideoPrimaryWarning = {
    attach: function (context, settings) {
      if(context === document) {
        $('#node-inline-video-form .form-submit, #node-inline-video-edit-form .form-submit').click(function(e){
          if($('#field_primary_inline_video_lib-media-library-wrapper-field_image_inline_video-0-subform .media-library-selection .media-library-item').length) {
            console.log("naa sulod");
          } else {
            console.log("wala sulod");
            e.preventDefault()
            alert("No Primary Inline Video Image Selected/Uploaded. Please upload/select an Inline Video Image.");
            console.log("gi click");
          }
          
          
        })
      }
    }
  }


}(jQuery, Drupal, Drupal.window));