/**
 * @file
 * Defines the behaviors needed for cropper integration.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.NewsCharCount = {
    attach: function(context, settings) {

      var short_title_field = $('#edit-field-short-title-0-value', context);
      // var summary_field = $('[data-drupal-selector="edit-body-0-summary"]', context);

      if (short_title_field.parent().find(".info").length == 0) {
        var info_short_title_field = $('<span class="info"></span>').appendTo(short_title_field.parent());
        info_short_title_field.html("The “short title” value, also known as a “Web Headline” is used when displaying content on compact settings. The value in this field should be 40 characters or less.");
      }

      // if (summary_field.parent().prev().find(".info").length == 0) {
      //   var info_summary_field = $('<span class="info summary-info"></span>').appendTo(summary_field.parent().prev());
      // }

      var sim_title_elems = null, allow_flag = true;
      var default_height_title = 20, default_max_height_title = default_height_title * 2;
      var res = new Array();

      // $('<span class="info"></span>').appendTo(summary_field.find('label'));
      sim_title_elems = new Array($(".tests-title .test-1"),$(".tests-title .test-2"));

      function toggleClass(elem, cls, replace) {
        elem.removeClass(cls).addClass(replace);
      }

      function updateInfo(suffix, text, info_elem) {
        var l = text.length;
        if (info_elem !== undefined) {
          info_elem.html(l + (l == 1 ? ' char' : ' chars') + ', ' + suffix);
        }
      }

      function simulation_title_checks(text){
        for(var i = 0; i < sim_title_elems.length; i++){
          res[i] = simulate($(sim_title_elems[i]), text, default_max_height_title);
        }
        allow_flag = res[0] && res[1];
        return allow_flag;
      }

      function simulate(elem, text, max_height){
        $(elem).text(text);
        return $(elem).height() <= max_height;
      }

      function track_title() {
        var me = $(this);
        setTimeout(function() {
          var title = me.val();
          var simulation_flag = !simulation_title_checks(title);
          if (title.length < 22 || title.length > 40) {//simulation_flag){//title.length > 86) {
            if (title.length < 22) {
              updateInfo("title too short!", title, info_short_title_field);
            }
            else {
              updateInfo("title too big!", title, info_short_title_field);
            }
            toggleClass(short_title_field.parent(),'ok', 'has-error');
          }
          else {
            updateInfo("good length!", title, info_short_title_field);
            toggleClass(short_title_field.parent(),'has-error', 'ok');
          }
        }, 100);
      }

      // function track_summary() {
      //   setTimeout(function() {
      //     var summary = summary_field.val();
      //     if (summary.length < 42 || summary.length > 80) {//simulation_flag){//title.length > 86) {
      //       if (summary.length < 42) {
      //         updateInfo("summary too short!", summary, info_summary_field);
      //       }
      //       else {
      //         updateInfo("summary too big!", summary, info_summary_field);
      //       }
      //       toggleClass(summary_field.parent().prev(),'ok', 'has-error');
      //     }
      //     else {
      //       updateInfo("good length!", summary, info_summary_field);
      //       toggleClass(summary_field.parent().prev(),'has-error', 'ok');
      //     }
      //   }, 100);
      // }

      short_title_field.once().bind('keydown', track_title);
      short_title_field.once().bind('change', track_title);
      if (short_title_field.val()) {
        track_title.apply(short_title_field.get(0));
      }

      // summary_field.once().bind('keydown', track_summary);
      // summary_field.once().bind('change', track_summary);

      // if (summary_field.val()) {
      //   track_summary.apply(summary_field.get(0));
      // }

    }
  };

}(jQuery, Drupal));
