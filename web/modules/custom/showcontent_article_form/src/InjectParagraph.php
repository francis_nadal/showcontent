<?php

namespace Drupal\showcontent_article_form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget;

class InjectParagraph {

  public static function addParagraphSubmit(array $form, FormStateInterface $form_state)
  {
    // Set the paragraph field in question.
    $paragraphFieldName = 'field_image_library_paragraphs';
    $paragraphName = 'image_library_custom_fields';
    $nodeFieldName = 'field_image_library';

    // Extract the paragraph field from the form.
    $element = NestedArray::getValue($form, [$paragraphFieldName, 'widget']);
    $field_name = $element['#field_name'];
    $field_parents = $element['#field_parents'];

    // Get the widget state.
    $widget_state = static::getWidgetState($field_parents, $field_name, $form_state);

    // Extract the images from the user input.
    $inputs = $form_state->getUserInput();
    $images = $inputs[$nodeFieldName];

    // Set a starting delta value based on 0 or the number of paragraphs already present.
    $startDelta = 0;
    if (isset($widget_state['paragraphs'])) {
      $startDelta = count($widget_state['paragraphs']);
    }

    // For every image found add a placeholder to inject the paragraphs later.
    foreach ($images['selection'] as $image) {
      $widget_state['inject_paragraphs'][$startDelta] = $paragraphName;
      $widget_state['image'][$startDelta] = $image;
      $widget_state['items_count']++;
      $startDelta++;
    }

    static::setWidgetState($field_parents, $field_name, $form_state, $widget_state);

    // Always rebuild the form.
    $form_state->setRebuild();
  }

  public static function addParagraphAjax(array $form, FormStateInterface $form_state) //
  {
    // Set the paragraph field in question.
    $paragraphFieldName = 'field_image_library_paragraphs';

    // Extract the paragraph field from the form.
    $element = NestedArray::getValue($form, [$paragraphFieldName, 'widget']);

    // Update the field with the needed Ajax formatting.
    $delta = $element['#max_delta'];
    $element[$delta]['#prefix'] = '<div class="ajax-new-content">' . (isset($element[$delta]['#prefix']) ? $element[$delta]['#prefix'] : '');
    $element[$delta]['#suffix'] = (isset($element[$delta]['#suffix']) ? $element[$delta]['#suffix'] : '') . '</div>';

    // Clear the add more delta from the paragraph module.
    NestedArray::setValue(
      $element,
      ['add_more', 'add_more_delta', '#value'],
      ''
    );

    // Return the paragraph element.
    return $element;
  }

  public static function getWidgetState(array $parents, $field_name, FormStateInterface $form_state) {
    // \Drupal::logger('getWidgetState')->notice('<pre><code>' . print_r("testing ra", 1) . '</code></pre>');
    return NestedArray::getValue($form_state->getStorage(), array_merge(['field_storage', '#parents'], $parents, ['#fields', $field_name]));
  }

  public static function setWidgetState(array $parents, $field_name, FormStateInterface $form_state, array $field_state) {
    // \Drupal::logger('setWidgetState')->notice('<pre><code>' . print_r("testing ra", 1) . '</code></pre>');
    NestedArray::setValue($form_state->getStorage(), array_merge(['field_storage', '#parents'], $parents, ['#fields', $field_name]), $field_state);
  }
}
