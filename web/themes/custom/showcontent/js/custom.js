/* --------------------------------------------- 

* Filename:     custom-style.js
* Version:      1.0.0 (2019-01-22)
* Website:      https://www.zymphonies.com
* Description:  System JS
* Author:       Zymphonies Team
                info@zymphonies.com

-----------------------------------------------*/

(function($){

	//Main menu
	$('#main-menu').smartmenus();
	
	//Mobile menu toggle
	$('.navbar-toggle').click(function(){
		$('.region-primary-menu').slideToggle();
	});

	//Mobile dropdown menu
	if ( $(window).width() < 767) {
		$(".region-primary-menu li a:not(.has-submenu)").click(function () {
			$('.region-primary-menu').hide();
	    });
	}

	//flexslider
	jQuery('.flexslider').flexslider({
    	animation: "slide"	
    });

	/**
	 * picturelist page
	 */
	
	Drupal.behaviors.csvDownload = {
    attach: function (context) {
      if (context === document) {
        if($('body').hasClass('page-node-type-page') || $('body').hasClass('path-picturelist')) {
					console.log("behaviors");
					// console.log($("#date_from").val());
					// console.log($("#date_to").val());
          $("#picture_list").click(function(){
						// console.log($("#date_from").val());
						// console.log($("#date_to").val());
            $(location).attr("href", "picturelist?created_from="+$("#date_from").val()+"+00%3A00%3A00&created_to="+$("#date_to").val()+"+23%3A59%3A59");
          });
        }
      }
    }
  }

}(jQuery, Drupal, window));